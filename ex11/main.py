# !/usr/bin/env python
"""
Ex11 main code.
"""

__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def ex11():
    """
    Compute combinations of length=2 of strings 'abc' and 'de'.

    :return: the list of combinations.
    """
    return [x+y for x in 'abc' for y in 'de']


if __name__ == '__main__':
    print(ex11())
