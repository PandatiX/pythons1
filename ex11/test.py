#!/usr/bin/env python

import unittest

from ex11.main import ex11


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx11(unittest.TestCase):
    def testEx11(self):
        # Test OK
        self.assertEqual(['ad', 'ae', 'bd', 'be', 'cd', 'ce'], ex11())


if __name__ == '__main__':
    unittest.main()
