#!/usr/bin/env python

import ast
import os
import sys
import unittest

from ex12.main import ex12


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx12(unittest.TestCase):
    def testEx12(self):
        # Write the ex12 output in a file
        filename = 'tmp.txt'
        with open(filename, 'w') as f:
            ostdout, sys.stdout = sys.stdout, f
            ex12()
            sys.stdout = ostdout
        f.close()
        # Test the values in this file
        with open(filename, 'r') as f:
            self.assertEqual({'a', 'b', 'c', 'd'}, ast.literal_eval(
                f.readline().replace('\n', '')[4:]))
            self.assertEqual({'s', 'b', 'd'}, ast.literal_eval(
                f.readline().replace('\n', '')[4:]))
            self.assertEqual('True', f.readline().replace('\n', '')[9:])
            self.assertEqual('False', f.readline().replace('\n', '')[9:])
            self.assertEqual({'a', 'c'}, ast.literal_eval(
                f.readline().replace('\n', '')[8:]))
            self.assertEqual({'s'}, ast.literal_eval(
                f.readline().replace('\n', '')[8:]))
            self.assertEqual({'a', 'b', 'c', 'd', 's'}, ast.literal_eval(
                f.readline().replace('\n', '')[12:]))
            self.assertEqual({'b', 'd'}, ast.literal_eval(
                f.readline().replace('\n', '')[12:]))
        f.close()
        os.remove(filename)


if __name__ == '__main__':
    unittest.main()
