#!/usr/bin/env python
"""
Ex12 main code.
"""

__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def ex12():
    """
    Compute the process for the two sets: X = {'a', 'b', 'c', 'd'} and
    Y = {'s', 'b', 'd'} and write each step in
    stdout.

    The process results in the display of the following actions:
     - The initial sets ;
     - The test of 'c' in X ;
     - The test of 'a' in Y ;
     - The following sets : X-Y and Y-X ;
     - The union of X and Y ;
     - The intersection of X and Y.
    """
    X = {'a', 'b', 'c', 'd'}
    Y = {'s', 'b', 'd'}

    # Display the initial sets
    print(f'X : {X}')
    print(f'Y : {Y}')

    # "c" in X
    print(f'c in X : {"c" in X}')

    # "a" in Y
    print(f'a in Y : {"a" in Y}')

    # Write X-Y and Y-X
    print(f'X - Y : {X - Y}')
    print(f'Y - X : {Y - X}')

    # Write the union of X and Y
    print(f'X union Y : {X | Y}')

    # Display the intersection of X and Y
    print(f'X inter Y : {X & Y}')


if __name__ == '__main__':
    ex12()
