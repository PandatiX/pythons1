#!/usr/bin/env python

import math
import os
import sys
import unittest

from ex07.main import maFonction, tabuler


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx07(unittest.TestCase):
    def testMaFonction(self):
        # Test OK
        self.assertEqual(-5, maFonction(0))
        self.assertEqual(-260, maFonction(-5))
        self.assertEqual(-35152265, maFonction(-260))
        self.assertEqual(math.inf, maFonction(math.inf))
        self.assertEqual(-math.inf, maFonction(-math.inf))
        self.assertTrue(math.isnan(maFonction(math.nan)))

        # Test ERROR
        with self.assertRaises(TypeError):
            maFonction(f'f')

    def testTabuler(self):
        # Test OK
        self.assertEqual([-2], tabuler(f'maFonction', 1, 2, 1))
        self.assertEqual([2005, 4402, 8203, 13732],
                         tabuler(f'maFonction', 10, 20, 3))
        self.assertEqual([], tabuler(f'maFonction', 1, 2, -1))
        # Injection POC
        filename = 'tmp.txt'
        with open(filename, 'w') as f:
            ostdout, sys.stdout = sys.stdout, f
            self.assertEqual([None], tabuler(f'print', 1, 2, 1))
            sys.stdout = ostdout
        f.close()
        with open(filename, 'r') as f:
            self.assertEqual('1', f.read(1))
        f.close()
        os.remove(filename)

        # Test ERROR
        with self.assertRaises(TypeError):      # Triggers matchSTR
            tabuler(0, 1, 2, 1)
        with self.assertRaises(ValueError):
            tabuler(f';', 1, 2, 1)
        with self.assertRaises(TypeError):      # Triggers 1st matchINT
            tabuler(f'maFonction', None, 2, 1)
        with self.assertRaises(TypeError):
            tabuler(f'maFonction', 1.0, 2, 1)
        with self.assertRaises(TypeError):
            tabuler(f'maFonction', f'1.0', 2, 1)
        with self.assertRaises(ValueError):
            tabuler(f'maFonction', -sys.maxsize - 2, 2, 1)
        with self.assertRaises(ValueError):
            tabuler(f'maFonction', sys.maxsize + 1, 2, 1)
        with self.assertRaises(TypeError):      # Triggers 2nd matchINT
            tabuler(f'maFonction', 1, None, 1)
        with self.assertRaises(TypeError):
            tabuler(f'maFonction', 1, 2.0, 1)
        with self.assertRaises(TypeError):
            tabuler(f'maFonction', 1, f'2.0', 1)
        with self.assertRaises(ValueError):
            tabuler(f'maFonction', 1, -sys.maxsize - 2, 1)
        with self.assertRaises(ValueError):
            tabuler(f'maFonction', 1, sys.maxsize + 1, 1)
        with self.assertRaises(TypeError):      # Triggers 3rd matchINT
            tabuler(f'maFonction', 1, 2, None)
        with self.assertRaises(TypeError):
            tabuler(f'maFonction', 1, 2, 1.0)
        with self.assertRaises(TypeError):
            tabuler(f'maFonction', 1, 2, f'1.0')
        with self.assertRaises(ValueError):
            tabuler(f'maFonction', 1, 2, -sys.maxsize - 2)
        with self.assertRaises(ValueError):
            tabuler(f'maFonction', 1, 2, sys.maxsize + 1)
        with self.assertRaises(ValueError):     # Triggers bound ordering
            tabuler(f'maFonction', 2, 1, 1)
        with self.assertRaises(ValueError):     # Triggers range step
            tabuler(f'maFonction', 1, 2, 0)


if __name__ == '__main__':
    unittest.main()
