#!/usr/bin/env python
"""
Ex09 main code.
"""

__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def ex09():
    """
    Compute the process for the list [17, 38, 10, 25, 72] and display each
    step in stdout.

    The process is the following:
     - Sort ;
     - Add 12 ;
     - Reverse ;
     - Index of 17 ;
     - Remove 38 ;
     - Sublist from 2nd to 3rd value (included) ;
     - Sublist from beginning to 2nd value (included) ;
     - Sublist from 3rd value (included) to end ;
     - Sublist of the whole list ;
     - Last value.
    """
    liste = [17, 38, 10, 25, 72]

    # Sort and display
    liste.sort()
    print(liste)

    # Add 12 and display
    liste.append(12)
    print(liste)

    # Reverse and display
    liste.reverse()
    print(liste)

    # Display index of 17
    print(liste.index(17))

    # Remove 38 and display
    liste.remove(38)
    print(liste)

    # Display sublist from 2nd to 3rd value (included)
    print(liste[2:3 + 1])

    # Display sublist from beginning to 2nd value (included)
    print(liste[:2 + 1])

    # Display sublist from 3rd value (included) to end
    print(liste[3 - 1:])

    # Display the sublist of the whole list
    print(liste[:])

    # Display last value
    print(liste[-1])


if __name__ == '__main__':
    ex09()
