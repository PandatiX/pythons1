#!/usr/bin/env python

import os
import sys
import unittest

from ex09.main import ex09


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx09(unittest.TestCase):
    def testEx09(self):
        # Write the ex09 output in a file
        filename = 'tmp.txt'
        with open(filename, 'w') as f:
            ostdout, sys.stdout = sys.stdout, f
            ex09()
            sys.stdout = ostdout
        f.close()
        # Test the values in this file
        with open(filename, 'r') as f:
            self.assertEqual('[10, 17, 25, 38, 72]',
                             f.readline().replace('\n', ''))
            self.assertEqual('[10, 17, 25, 38, 72, 12]',
                             f.readline().replace('\n', ''))
            self.assertEqual('[12, 72, 38, 25, 17, 10]',
                             f.readline().replace('\n', ''))
            self.assertEqual('4', f.readline().replace('\n', ''))
            self.assertEqual('[12, 72, 25, 17, 10]',
                             f.readline().replace('\n', ''))
            self.assertEqual('[25, 17]', f.readline().replace('\n', ''))
            self.assertEqual('[12, 72, 25]', f.readline().replace('\n', ''))
            self.assertEqual('[25, 17, 10]', f.readline().replace('\n', ''))
            self.assertEqual('[12, 72, 25, 17, 10]',
                             f.readline().replace('\n', ''))
            self.assertEqual('10', f.readline().replace('\n', ''))
        f.close()
        os.remove(filename)


if __name__ == '__main__':
    unittest.main()
