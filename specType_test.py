import math
import unittest
import sys

import specType


class TestSpecType(unittest.TestCase):
    def testToSTR(self):
        # Test OK
        self.assertEqual(f'3.:', specType.toSTR(f'3;.:'))
        self.assertEqual(f'', specType.toSTR(f';;;'))
        self.assertEqual(f'', specType.toSTR(f''))

        # Test KO
        # No understanding mistake of "point-virgule"
        self.assertNotEqual(f'.,', specType.toSTR(f';'))

        # Test ERROR
        with self.assertRaises(TypeError):
            specType.toSTR(None)
        with self.assertRaises(TypeError):
            specType.toSTR(0)
        with self.assertRaises(TypeError):
            specType.toSTR(0.0)

    def testToINT(self):
        # Test OK
        self.assertEqual(0, specType.toINT(0))
        self.assertEqual(-sys.maxsize - 1, specType.toINT(-sys.maxsize - 2))
        self.assertEqual(sys.maxsize, specType.toINT(sys.maxsize + 1))

        # Test ERROR
        with self.assertRaises(TypeError):
            specType.toINT(None)
        with self.assertRaises(TypeError):
            specType.toINT(0.0)
        with self.assertRaises(TypeError):
            specType.toINT(f'0.0')

    def testToFLOAT(self):
        # Test OK
        self.assertEqual(sys.float_info.min, specType.toFLOAT(0.0))
        self.assertEqual(sys.float_info.max, specType.toFLOAT(math.inf))

        # Test ERROR
        with self.assertRaises(TypeError):
            specType.toFLOAT(None)
        with self.assertRaises(TypeError):
            specType.toFLOAT(0)
        with self.assertRaises(TypeError):
            specType.toFLOAT(f'0.0')
        with self.assertRaises(TypeError):
            specType.toFLOAT(math.nan)

    def testToFLOAT2(self):
        # Test OK
        # 0.0 -> sys.float_info.min -> 0.0
        self.assertEqual(0.0, specType.toFLOAT2(0.0))
        self.assertEqual(sys.float_info.max, specType.toFLOAT2(math.inf))
        self.assertEqual(0.66666667, specType.toFLOAT2(2/3))

        # Test ERROR
        with self.assertRaises(TypeError):
            specType.toFLOAT2(None)
        with self.assertRaises(TypeError):
            specType.toFLOAT2(0)
        with self.assertRaises(TypeError):
            specType.toFLOAT2(f'0.0')
        with self.assertRaises(TypeError):
            specType.toFLOAT2(math.nan)

    def testMatchSTR(self):
        # Test ERROR
        with self.assertRaises(TypeError):
            specType.matchSTR(None)
        with self.assertRaises(TypeError):
            specType.matchSTR(0)
        with self.assertRaises(TypeError):
            specType.matchSTR(0.0)
        with self.assertRaises(ValueError):
            specType.matchSTR(f'a;b')       # Detect only one
        with self.assertRaises(ValueError):
            specType.matchSTR(f';;')        # Detect multiple

    def testMatchINT(self):
        # Test ERROR
        with self.assertRaises(TypeError):
            specType.matchINT(None)
        with self.assertRaises(TypeError):
            specType.matchINT(0.0)
        with self.assertRaises(TypeError):
            specType.matchINT(f'0.0')
        with self.assertRaises(ValueError):
            specType.matchINT(-sys.maxsize - 2)
        with self.assertRaises(ValueError):
            specType.matchINT(sys.maxsize + 1)

    def testMatchFLOAT(self):
        # Test ERROR
        with self.assertRaises(TypeError):
            specType.matchFLOAT(math.nan)
        with self.assertRaises(TypeError):
            specType.matchFLOAT(None)
        with self.assertRaises(TypeError):
            specType.matchFLOAT(1)
        with self.assertRaises(TypeError):
            specType.matchFLOAT(f'1.0')
        with self.assertRaises(ValueError):
            specType.matchFLOAT(0.0)
        with self.assertRaises(ValueError):
            specType.matchFLOAT(math.inf)

    def testMathFLOAT2(self):
        # Test ERROR
        with self.assertRaises(TypeError):
            specType.matchFLOAT2(None)
        with self.assertRaises(TypeError):
            specType.matchFLOAT2(1)
        with self.assertRaises(TypeError):
            specType.matchFLOAT2(f'1.0')
        with self.assertRaises(TypeError):
            specType.matchFLOAT2(math.nan)
        with self.assertRaises(ValueError):
            specType.matchFLOAT2(math.inf)
        with self.assertRaises(ValueError):
            specType.matchFLOAT2(0.0)
        with self.assertRaises(ValueError):
            specType.matchFLOAT2(0.333333333)

    def testFLOAT2toSTR(self):
        # Test OK
        self.assertEqual(f'1', specType.FLOAT2toSTR(1.0))
        self.assertEqual(f'100000000000000000000',
                         specType.FLOAT2toSTR(1.0e20))
        self.assertEqual(f'17976931348623157081452742373170435679807056752584'
                         f'49965989174768031572607800285387605895586327668781'
                         f'71540458953514382464234321326889464182768467546703'
                         f'53751698604991057655128207624549009038932894407586'
                         f'85084551339423045832369032229481658085593321233482'
                         f'74797826204144723168738177180919299881250404026184'
                         f'124858368',
                         specType.FLOAT2toSTR(sys.float_info.max))
        self.assertEqual(f'1.23456789', specType.FLOAT2toSTR(1.234567894))
        # Because of FLOAT2 specification rounding mistake, can't work
        self.assertEqual(f'0.', specType.FLOAT2toSTR(sys.float_info.min))


if __name__ == '__main__':
    unittest.main()
