#!/usr/bin/env python

import os
import sys
import unittest

from ex10.main import ex10


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx10(unittest.TestCase):
    def testEx10(self):
        # Write the ex10 output in a file
        filename = 'tmp.txt'
        with open(filename, 'w') as f:
            ostdout, sys.stdout = sys.stdout, f
            ex10()
            sys.stdout = ostdout
        f.close()
        # Test the values in this file
        with open(filename, 'r') as f:
            self.assertEqual(f'[0, 1, 5, 6, 7, 8]',
                             f.readline().replace('\n', ''))
        f.close()
        os.remove(filename)


if __name__ == '__main__':
    unittest.main()
