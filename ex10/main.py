#!/usr/bin/env python
"""
Ex10 main code.
"""

__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def ex10():
    """
    Apply a comprehension list on [0, 1, 2, 3, 4, 5] to add 3 to each n >= 2,
    and write the result in stdout.
    """
    liste = [0, 1, 2, 3, 4, 5]

    liste = [x+3 if x >= 2 else x for x in liste]
    print(liste)


if __name__ == '__main__':
    ex10()
