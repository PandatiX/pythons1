#!/usr/bin/env python

import unittest

from ex03.main import ex03


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx03(unittest.TestCase):
    def testEx03(self):
        # Test OK
        self.assertEqual(f'', ex03(f'-', f''))
        self.assertEqual(f'a', ex03(f'a', f'b'))
        self.assertEqual(f'1', ex03(f'a', f'1'))
        self.assertEqual(f'û', ex03(f'û', f'ûû'))
        self.assertEqual(f'a-a', ex03(f'aa', f'a-a'))
        self.assertEqual(f'a-a', ex03(f'a-a', f'aa'))
        self.assertEqual(f'I\'m a pOtAtO', ex03(f'You can\'t be a potato'
                                                f'John...', f'I\'m a pOtAtO'))

        # Test ERROR
        with self.assertRaises(TypeError):      # Triggers 1st matchSTR
            ex03(None, f'a')
        with self.assertRaises(TypeError):
            ex03(0, f'a')
        with self.assertRaises(TypeError):
            ex03(0.0, f'a')
        with self.assertRaises(ValueError):
            ex03(f'tom;ato', f'a')
        with self.assertRaises(TypeError):      # Triggers 2nd matchSTR
            ex03(f'a', None)
        with self.assertRaises(TypeError):
            ex03(f'a', 0)
        with self.assertRaises(TypeError):
            ex03(f'a', 0.0)
        with self.assertRaises(ValueError):
            ex03(f'a', f'tom;ato')


if __name__ == '__main__':
    unittest.main()
