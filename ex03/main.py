#!/usr/bin/env python
"""
Ex03 main code.
"""

import csv
import sys

sys.path.append("..")
import specType  # noqa


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def replaceAccentuatedChars(s: str):
    """
    Replace French accentuated chars of the string to their respective basic
    char (i.e. 'à' becomes 'a').

    :param s: (str) the string to replace within.
    :return: the string with no French accentuated chars.
    """
    s.replace('à', 'a')
    s.replace('â', 'a')
    s.replace('é', 'e')
    s.replace('è', 'e')
    s.replace('ê', 'e')
    s.replace('î', 'i')
    s.replace('ô', 'o')
    s.replace('ù', 'u')
    s.replace('ç', 'c')
    return s


def removeUnimportantChars(s: str):
    """
    Remove unimportant chars in the string.

    :param s: the string to remove those chars within.
    :return: the string without those unimportant chars.
    """
    s.replace('-', '')
    s.replace('\'', '')
    s.replace('`', '')
    s.replace('"', '')
    return s


def modifySTRToSpecification(s: str):
    """
    Apply all the transformations on strings to be used by ex03.

    :param s: the str to transform.
    :return: the transformed str.
    """
    s.lower()
    s = replaceAccentuatedChars(s)
    s = removeUnimportantChars(s)
    return s


def ex03(s1: str, s2: str):
    """
    Compute the min between two str (lexicographical order).
    Notice that numbers and special characters (except semicolon) are allowed
    in STR specification.

    :param s1: (STR) first string.
    :param s2: (STR) second string.
    :return: (STR) the min string.
    :raise TypeError: if any input is not of type str.
    :raise ValueError: if any input contains a semicolon (';').
    """
    specType.matchSTR(s1)
    specType.matchSTR(s2)
    # Convert STR to the specification
    s1c = modifySTRToSpecification(s1)
    s2c = modifySTRToSpecification(s2)

    if min(s1c, s2c) == s1c:
        return s1
    return s2


if __name__ == '__main__':
    try:
        # Set file input
        if len(sys.argv) != 2:
            raise ValueError
        else:
            filename = sys.argv[1]

        # Parse file and evaluate input
        with open(filename, newline='') as csvFile:
            reader = csv.reader(csvFile, delimiter=';')
            for row in reader:
                if len(row) != 2:
                    raise ValueError
                try:
                    print(ex03(row[0], row[1]))
                except TypeError:       # Argument is not a STR
                    print(f'ERROR')
                except ValueError:      # Argument contains ';'
                    print(f'ERROR')
        csvFile.close()

    except FileNotFoundError:   # Provided file name is not valid
        print(f'ERROR')
    except ValueError:          # Did not provide file name/incorrect struct
        print(f'ERROR')
