#!/usr/bin/env python

import math
import sys
import unittest

from ex06.main import cube, volumeSphere


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx06(unittest.TestCase):
    def testCube(self):
        # Test OK
        self.assertEqual(1.0, cube(1.0))
        self.assertEqual(8.0, cube(2.0))

        # Test KO
        # Won't match FLOAT or FLOAT2 specification (higher than sup bound)
        self.assertNotEqual(sys.float_info.max,
                            cube(sys.float_info.max ** (1/3) + 7.41e+88))

        # Test ERROR
        with self.assertRaises(TypeError):
            cube(math.nan)
        with self.assertRaises(TypeError):
            cube(None)
        with self.assertRaises(TypeError):
            cube(1)
        with self.assertRaises(TypeError):
            cube(f'1.0')
        with self.assertRaises(ValueError):
            cube(0.0)
        with self.assertRaises(ValueError):
            cube(math.inf)
        with self.assertRaises(OverflowError):
            cube(sys.float_info.max)

    def testVolumeSphere(self):
        # Test OK
        self.assertEqual(4.1887902, volumeSphere(1.0))

        # Test KO
        # Not matching FLOAT spec (number of decimals)
        self.assertNotEqual(4.1887902047863905, volumeSphere(1.0))
        # Out of bounds (higher)
        self.assertNotEqual(sys.float_info.max,
                            sys.float_info.max ** (1/3) + 7.41e+88)

        # Test ERROR
        with self.assertRaises(TypeError):
            volumeSphere(math.nan)
        with self.assertRaises(TypeError):
            volumeSphere(None)
        with self.assertRaises(TypeError):
            volumeSphere(1)
        with self.assertRaises(TypeError):
            volumeSphere(f'1.0')
        with self.assertRaises(ValueError):
            volumeSphere(0.0)
        with self.assertRaises(ValueError):
            volumeSphere(math.inf)
        with self.assertRaises(OverflowError):
            volumeSphere(sys.float_info.max)


if __name__ == '__main__':
    unittest.main()
