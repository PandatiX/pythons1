#!/usr/bin/env python
"""
Ex06 main code.
"""

import csv
import sys
import math

sys.path.append("..")
import specType  # noqa


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def cube(n: float):
    """
    Return the cube of a FLOAT.

    :param n: (FLOAT) number to set to the cube.
    :return: (FLOAT) cube of n.
    :raise TypeError: if any input doesn't match FLOAT specification.
    :raise ValueError: if any input is out of bounds (not in
        [sys.float_info.min;sys.float_info.max]).
    :raise OverflowError: if n is too big
        (>~ sys.float_info.max^(1/3)+7.41e+88).
    """
    specType.matchFLOAT(n)
    return n ** 3


def volumeSphere(r: float):
    """
    Return the volume of a sphere with a radius r.

    :param r: (FLOAT) radius of the sphere.
    :return: (FLOAT2) volume of the sphere.
    :raise TypeError: if any input doesn't match FLOAT specification.
    :raise ValueError: if any input is out of bounds (not in
        [sys.float_info.min;sys.float_info.max]).
    :raise OverflowError: if n is too big
        (>~ sys.float_info.max^(1/3)+7.41e+88).
    """
    n = cube(r)
    v = (4 / 3) * math.pi * n
    return specType.toFLOAT2(v)


if __name__ == '__main__':
    try:
        # Set file input
        if len(sys.argv) != 2:
            raise ValueError
        else:
            filename = sys.argv[1]

        # Parse file and evaluate input
        with open(filename, newline='') as csvFile:
            reader = csv.reader(csvFile, delimiter=';')
            for row in reader:
                if len(row) != 1:
                    raise ValueError
                try:
                    print(specType.FLOAT2toSTR(
                        volumeSphere(float(row[0]))
                    ))
                except TypeError:           # Argument is not a FLOAT
                    print(f'ERROR')
                except ValueError:          # Value out of bounds
                    print(f'ERROR')
                except OverflowError:       # Value was too big
                    print(f'ERROR')
        csvFile.close()

    except FileNotFoundError:   # Provided file name is not valid
        print(f'ERROR')
    except ValueError:          # Did not provide file name/incorrect struct
        print(f'ERROR')
