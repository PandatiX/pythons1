<div align="center">
    <img src="res/logo.png"/>
    <h1>PythonS1</h1>
</div>

[PythonS1](https://gitlab.com/PandatiX/pythons1) is the Python (3.8) module project for the 1st semester at the ENSIBS
of:
 - Lucas TESSON, e2004675 ;
 - Nolan GLOUX, e2004726.

[![pipeline status](https://gitlab.com/PandatiX/pythons1/badges/master/pipeline.svg)](https://gitlab.com/PandatiX/pythons1/-/commits/master)
[![coverage report](https://gitlab.com/PandatiX/pythons1/badges/master/coverage.svg)](https://gitlab.com/PandatiX/pythons1/commits/master)

Note that the code developed was designed in compliance with the usual secure development standards: type checks are
performed in the functions (details in the documentation of each), in order to respect the concepts of black box. This
choice is contrary to the indications of development, but have been considered more realistic and safe.

As tests are considered as for developers, they are not documented (but commented).

## About chosen tools
We have chosen tools which doesn't rely on installing something on the dev side,
but some of those (particularly Coverage.py) are specially for the forge (CI/CD).
Thus, the tools we are using are:
 - [GitLab](https://gitlab.com/), for collaborative works and CI/CD ;
 - [Sphinx](https://www.sphinx-doc.org/en/master/), to build the doc ;
 - [GitLabPages](https://pandatix.gitlab.io/pythons1/), to publish documentation online ;
 - [Unittest](https://docs.python.org/3/library/unittest.html), to test our code ;
 - [Coverage.py](https://coverage.readthedocs.io/en/coverage-5.3/), to measure code coverage ;
 - [PyCodeStyle](https://pypi.org/project/pycodestyle/), to ensure PEP8 code validation ;
 - [Discord](https://discord.gg/nTVV2Nv), to easily communicate ;
 - [Yappy, the GitLab Monitor](https://top.gg/bot/303661490114789391), to get instantaneous news on Discord about the forge status.

## Manually run tests
After cloning the repo, to run tests for exercises, run:
```shell script
pushd <repoDir>
python -m unittest discover
```
To run tests for exercises AND specType, run:
```shell script
pushd <repoDir>
python -m unittest discover -p "*test.py"
```
Automated code coverage is using this last strategy.

## Manually do other tasks
Please refer to .gitlab-ci.yml file.
