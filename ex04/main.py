#!/usr/bin/env python
"""
Ex04 main code.
"""

import csv
import sys

sys.path.append("..")
import specType  # noqa


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


PSEUIL = 2.3
VSEUIL = 7.41


def ex04(pression: float, volume: float):
    """
    Get the action to achieve on the volume of the pressurized enclosure.

    :param pression: (FLOAT) pressure in the pressurized enclosure.
    :param volume: (FLOAT) volume of the pressurized enclosure.
    :return: ({"KO","Augmenter","Diminuer","OK"}) action to achieve.
    :raise TypeError: if any input doesn't match FLOAT specification.
    :raise ValueError: if any input is out of bounds (not in
        [sys.float_info.min;sys.float_info.max]).
    """
    specType.matchFLOAT(pression)
    specType.matchFLOAT(volume)

    if pression > PSEUIL and volume > VSEUIL:
        return f'KO'
    elif pression > PSEUIL:
        return f'Augmenter'
    elif volume > VSEUIL:
        return f'Diminuer'
    else:
        return f'OK'


if __name__ == '__main__':
    try:
        # Set file input
        if len(sys.argv) != 2:
            raise ValueError
        else:
            filename = sys.argv[1]

        # Parse file and evaluate input
        with open(filename, newline='') as csvFile:
            reader = csv.reader(csvFile, delimiter=';')
            for row in reader:
                if len(row) != 2:
                    raise ValueError
                try:
                    print(ex04(float(row[0]), float(row[1])))
                except TypeError:           # Argument is not a FLOAT
                    print(f'ERROR')
                except ValueError:          # Value out of bounds
                    print(f'ERROR')
        csvFile.close()

    except FileNotFoundError:   # Provided file name is not valid
        print(f'ERROR')
    except ValueError:          # Did not provide file name/incorrect struct
        print(f'ERROR')
