#!/usr/bin/env python

import math
import unittest

from ex04.main import ex04, VSEUIL, PSEUIL


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx04(unittest.TestCase):
    def testEx04(self):
        # Test OK
        self.assertEqual(f'KO', ex04(PSEUIL + 1, VSEUIL + 1))
        self.assertEqual(f'Augmenter', ex04(PSEUIL + 1, 1.0))
        self.assertEqual(f'Diminuer', ex04(1.0, VSEUIL + 1))
        self.assertEqual(f'OK', ex04(1.0, 1.0))

        # Test Error
        with self.assertRaises(TypeError):      # Triggers 1st matchFLOAT
            ex04(math.nan, 1.0)
        with self.assertRaises(TypeError):
            ex04(None, 1.0)
        with self.assertRaises(TypeError):
            ex04(1, 1.0)
        with self.assertRaises(TypeError):
            ex04(f'1.0', 1.0)
        with self.assertRaises(ValueError):
            ex04(0.0, 1.0)
        with self.assertRaises(ValueError):
            ex04(math.inf, 1.0)
        with self.assertRaises(TypeError):      # Triggers 2nd matchFLOAT
            ex04(1.0, math.nan)
        with self.assertRaises(TypeError):
            ex04(1.0, None)
        with self.assertRaises(TypeError):
            ex04(1.0, 1)
        with self.assertRaises(TypeError):
            ex04(1.0, f'1.0')
        with self.assertRaises(ValueError):
            ex04(1.0, 0.0)
        with self.assertRaises(ValueError):
            ex04(1.0, math.inf)


if __name__ == '__main__':
    print(f'ex04/test.py')
