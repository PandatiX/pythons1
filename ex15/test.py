#!/usr/bin/env python

import math
import unittest

from ex15.main import Vecteur2D


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx15(unittest.TestCase):
    def test__init__(self):
        # Test OK
        # Default constructor
        v = Vecteur2D()
        self.assertEqual(1.0, v._x)
        self.assertEqual(1.0, v._y)
        # Constructor with parameters
        v = Vecteur2D(3.14, 15.926)
        self.assertEqual(3.14, v._x)
        self.assertEqual(15.926, v._y)
        # No component inversion (i.e. argument value ordering)
        v = Vecteur2D(15.926, 3.14)
        self.assertEqual(15.926, v._x)
        self.assertEqual(3.14, v._y)
        # FLOAT and not FLOAT2 precision
        v = Vecteur2D(0.123456789, 9.012345674)
        self.assertEqual(0.123456789, v._x)
        self.assertEqual(9.012345674, v._y)

        # Test ERROR
        with self.assertRaises(TypeError):  # Triggers 1st matchFLOAT
            Vecteur2D(math.nan, 1.0)
        with self.assertRaises(TypeError):
            Vecteur2D(None, 1.0)
        with self.assertRaises(TypeError):
            Vecteur2D(1, 1.0)
        with self.assertRaises(TypeError):
            Vecteur2D(f'1.0', 1.0)
        with self.assertRaises(ValueError):
            Vecteur2D(0.0, 1.0)
        with self.assertRaises(ValueError):
            Vecteur2D(math.inf, 1.0)
        with self.assertRaises(TypeError):  # Triggers 2nd matchFLOAT
            Vecteur2D(1.0, math.nan)
        with self.assertRaises(TypeError):
            Vecteur2D(1.0, None)
        with self.assertRaises(TypeError):
            Vecteur2D(1.0, 1)
        with self.assertRaises(TypeError):
            Vecteur2D(1.0, f'1.0')
        with self.assertRaises(ValueError):
            Vecteur2D(1.0, 0.0)
        with self.assertRaises(ValueError):
            Vecteur2D(1.0, math.inf)

    def test__str__(self):
        # Test OK
        v = Vecteur2D()                             # Default value
        self.assertEqual(f'(x=1, y=1)', v.__str__())
        v = Vecteur2D(3.14, 15.926)                 # With parameters
        self.assertEqual(f'(x=3.14, y=15.926)', v.__str__())
        v = Vecteur2D(0.123456789, 9.012345674)     # FLOAT2 precision
        self.assertEqual(f'(x=0.12345679, y=9.01234567)', v.__str__())

        # Test ERROR
        v = Vecteur2D()
        with self.assertRaises(TypeError):      # Triggers 1st toFLOAT2
            v._x, v._y = None, 0.0
            v.__str__()
        with self.assertRaises(TypeError):
            v._x, v._y = 0, 0.0
            v.__str__()
        with self.assertRaises(TypeError):
            v._x, v._y = f'0.0', 0.0
            v.__str__()
        with self.assertRaises(TypeError):
            v._x, v._y = math.nan, 0.0
            v.__str__()
        with self.assertRaises(TypeError):      # Triggers 2nd toFLOAT2
            v._x, v._y = 0.0, None
            v.__str__()
        with self.assertRaises(TypeError):
            v._x, v._y = 0.0, 0
            v.__str__()
        with self.assertRaises(TypeError):
            v._x, v._y = 0.0, f'0.0'
            v.__str__()
        with self.assertRaises(TypeError):
            v._x, v._y = 0.0, math.nan
            v.__str__()


if __name__ == '__main__':
    unittest.main()
