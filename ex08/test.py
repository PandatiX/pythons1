#!/usr/bin/env python

import os
import sys
import unittest

from ex08.main import tchacatchac


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx08(unittest.TestCase):
    def testTchacatchac(self):
        # Test OK
        # Write the ex08 output in a file
        filename = 'tmp.txt'
        with open(filename, 'w') as f:
            ostdout, sys.stdout = sys.stdout, f
            for i in range(100, 300 + 1, 10):
                print(tchacatchac(i))
            sys.stdout = ostdout
        f.close()
        # Test the values in this file
        with open(filename, 'r') as f:
            self.assertEqual('10:41', f.readline().replace('\n', ''))
            self.assertEqual('10:32', f.readline().replace('\n', ''))
            self.assertEqual('10:24', f.readline().replace('\n', ''))
            self.assertEqual('10:18', f.readline().replace('\n', ''))
            self.assertEqual('10:12', f.readline().replace('\n', ''))
            self.assertEqual('10:07', f.readline().replace('\n', ''))
            self.assertEqual('10:03', f.readline().replace('\n', ''))
            self.assertEqual('10:00', f.readline().replace('\n', ''))
            self.assertEqual('9:56', f.readline().replace('\n', ''))
            self.assertEqual('9:53', f.readline().replace('\n', ''))
            self.assertEqual('9:50', f.readline().replace('\n', ''))
            self.assertEqual('9:48', f.readline().replace('\n', ''))
            self.assertEqual('9:46', f.readline().replace('\n', ''))
            self.assertEqual('9:44', f.readline().replace('\n', ''))
            self.assertEqual('9:42', f.readline().replace('\n', ''))
            self.assertEqual('9:40', f.readline().replace('\n', ''))
            self.assertEqual('9:39', f.readline().replace('\n', ''))
            self.assertEqual('9:37', f.readline().replace('\n', ''))
            self.assertEqual('9:36', f.readline().replace('\n', ''))
            self.assertEqual('9:35', f.readline().replace('\n', ''))
            self.assertEqual('9:33', f.readline().replace('\n', ''))
        f.close()
        os.remove(filename)

        # Test ERROR
        with self.assertRaises(TypeError):
            tchacatchac(None)
        with self.assertRaises(TypeError):
            tchacatchac(0.0)
        with self.assertRaises(TypeError):
            tchacatchac(f'0.0')
        with self.assertRaises(ValueError):
            tchacatchac(-sys.maxsize - 2)
        with self.assertRaises(ValueError):
            tchacatchac(sys.maxsize + 1)


if __name__ == '__main__':
    unittest.main()
