#!/usr/bin/env python
"""
Ex08 main code.
"""

import sys

sys.path.append("..")
import specType  # noqa


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


TRAIN_HOUR_DEPARTURE = 9
NORTH_STATION_TO_ARRAS_STATION = 170


def tchacatchac(s: int):
    """
    Return the drama hour based on the given speed.

    :param s: (INT) train speed, in km.h^-1.
    :return: (STR) drama hour, formatted as HH:MM.
    :raise TypeError: if input doesn't match INT specification.
    :raise ValueError: if any input is out of bounds (not in
        [-sys.maxsize - 1;sys.maxsize]).
    """
    specType.matchINT(s)
    timeImpact = TRAIN_HOUR_DEPARTURE + NORTH_STATION_TO_ARRAS_STATION / s
    hourImpact = int(timeImpact)
    minuteImpact = int((timeImpact - hourImpact) * 60)

    z = (f'', f'0')[minuteImpact < 10]
    return f'{int(hourImpact)}:{z}{minuteImpact}'


if __name__ == '__main__':
    try:
        for i in range(100, 300 + 1, 10):
            print(f'{i}km/h -> {tchacatchac(i)}')
    except TypeError:       # Argument is not an INT
        print(f'ERROR')
    except ValueError:      # Value out of bounds (int min/max)
        print(f'ERROR')
