#!/usr/bin/env python
"""
Ex16 main code.
"""

import csv
import sys

sys.path.append('..')
import specType  # noqa


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class Vecteur2D:
    """
    Class that defines a basic 2D vector with one operator overload (+)
    """
    def __init__(self, x=1.0, y=1.0):
        specType.matchFLOAT(x)
        specType.matchFLOAT(y)
        self._x = x
        self._y = y

    def __str__(self):
        x = specType.toFLOAT2(self._x)
        y = specType.toFLOAT2(self._y)
        return f'(x={specType.FLOAT2toSTR(x)}, y={specType.FLOAT2toSTR(y)})'

    def __add__(self, other):
        if not isinstance(other, Vecteur2D):
            raise TypeError
        x = self._x + other._x
        y = self._y + other._y
        return Vecteur2D(x, y)


if __name__ == '__main__':
    try:
        # Set file input
        if len(sys.argv) != 2:
            raise ValueError
        else:
            filename = sys.argv[1]

        # Parse file and evaluate input
        with open(filename, newline='') as csvFile:
            reader = csv.reader(csvFile, delimiter=';')
            for row in reader:
                if len(row) != 4:
                    raise ValueError
                try:
                    v1 = Vecteur2D(float(row[0]), float(row[1]))
                    v2 = Vecteur2D(float(row[2]), float(row[3]))
                    sommeVect = v1 + v2

                    print(f'v1 : {v1}')
                    print(f'v2 : {v2}')
                    print(f'somme : {sommeVect}')
                except TypeError:       # Argument is not a FLOAT
                    print(f'ERROR')
                except ValueError:      # Value out of bounds (float min/max)
                    print(f'ERROR')
        csvFile.close()

    except FileNotFoundError:   # Provided file name is not valid
        print(f'ERROR')
    except ValueError:          # Did not provide file name/incorrect struct
        print(f'ERROR')
