#!/usr/bin/env python
"""
Ex13 main code.
"""

import csv
import sys
from collections import Counter

sys.path.append('..')
import specType  # noqa


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def compterMots(inputStr: str):
    """
    Count words in a STR.

    :param inputStr: (STR) the string to count words in.
    :return: ({'STR': INT, ...}) the ordered dictionary of words in str.
        Notice it doesn't take into account letter case and common special
        characters.
    :raise ValueError: if any word has been counted too many times (over INT
        superior bound).
    """
    specType.matchSTR(inputStr)

    # Replace special characters to only count words
    # (i.e. 'W,' -> 'W' because we are counting W occurrences)
    regex = f'@#$%^&*()[]{{}}:,./<>?\\|`~=_'
    inputStr = inputStr.translate({ord(c): f'' for c in regex})

    # Lower and split in words
    words = inputStr.lower().split()

    # Count words and sort
    wordCount = Counter(words)
    d = dict(sorted(wordCount.items()))

    # Ensure INT match
    for key in d:
        specType.matchINT(d[key])

    return d


if __name__ == '__main__':
    try:
        # Set file input
        if len(sys.argv) != 2:
            raise ValueError
        else:
            filename = sys.argv[1]

        # Parse file and evaluate input
        with open(filename, newline='') as csvFile:
            reader = csv.reader(csvFile, delimiter=';')
            for row in reader:
                if len(row) != 1:
                    raise ValueError
                try:
                    print(compterMots(row[0]))
                except ValueError:      # Too many time the same word
                    print(f'ERROR')
        csvFile.close()

    except FileNotFoundError:   # Provided file name is not valid
        print(f'ERROR')
    except ValueError:          # Did not provide file name/incorrect struct
        print(f'ERROR')
