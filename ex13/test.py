#!/usr/bin/env python

import unittest

from ex13.main import compterMots


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx13(unittest.TestCase):
    def testCompterMots(self):
        # Test OK
        self.assertEqual({}, compterMots(f''))
        d = {'je': 2, 'suis': 1, 'tomate': 1, 'une': 1}
        self.assertEqual(d, compterMots(f'je je suis une tomate'))
        self.assertEqual(d, compterMots(f'#j*e je .<s[u]is %u&ne{{}} t_o?ma\\'
                                        f'te'))
        self.assertEqual(d, compterMots(f'  je je suis   une tomate    '))
        self.assertEqual(d, compterMots(f'Je jE SuIs uNe tOmAtE'))

        # Test ERROR
        # For obvious reasons (size), the test for too much word count is not
        # achieved and considered as valid.
        with self.assertRaises(TypeError):
            compterMots(None)
        with self.assertRaises(TypeError):
            compterMots(0)
        with self.assertRaises(TypeError):
            compterMots(0.0)
        with self.assertRaises(ValueError):
            compterMots(f'a;b')             # Detect only one
        with self.assertRaises(ValueError):
            compterMots(f';;')              # Detect multiple


if __name__ == '__main__':
    unittest.main()
