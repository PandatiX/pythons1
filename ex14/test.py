#!/usr/bin/env python

import os
import sys
import unittest

from ex14.main import MaClasse


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx14(unittest.TestCase):
    def testEx14(self):
        # Write the ex14 output in a file
        filename = 'tmp.txt'
        with open(filename, 'w') as f:
            ostdout, sys.stdout = sys.stdout, f
            MaClasse().affiche()
            sys.stdout = ostdout
        f.close()
        # Test the values in this file
        with open(filename, 'r') as f:
            self.assertEqual(f'(28, 42)', f.readline().replace('\n', ''))
        f.close()
        os.remove(filename)


if __name__ == '__main__':
    unittest.main()
