#!/usr/bin/env python
"""
Ex14 main code.
"""

import sys

sys.path.append('..')
import specType  # noqa


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class MaClasse:
    """
    Class to display two hardcoded INT.
    """
    def __init__(self):
        x = 23
        y = x + 5
        specType.matchINT(x)
        specType.matchINT(y)
        self._x = x
        self._y = y

    def affiche(self):
        """
        Write two hardcoded values (formatted as (INT, INT)) in stdout.
        """
        z = 42  # How could this be an instance attribute and not in __init__?
        print(f'({self._y}, {z})')


if __name__ == '__main__':
    objet = MaClasse()
    objet.affiche()
