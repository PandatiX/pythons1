#!/usr/bin/env python
"""
Module to centralize the specification implementation.
"""

import sys
import math


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def toSTR(s: str):
    """
    Convert a native str type to the STR type in specification.

    :param s: the native str.
    :return: the STR type according to the specification.
    :raise TypeError: if s is not a str.
    """
    if not isinstance(s, str):
        raise TypeError
    return s.replace(';', '')


def toINT(i: int):
    """
    Convert a native int to the INT type in specification.

    :param i: the native int.
    :return: the INT type according to the specification.
    :raise TypeError: if i is not an int.
    """
    if not isinstance(i, int):
        raise TypeError
    if i < -sys.maxsize - 1:
        return -sys.maxsize - 1
    if i > sys.maxsize:
        return sys.maxsize
    return i


def toFLOAT(f: float):
    """
    Convert a native float type to the FLOAT type in specification.

    :param f: the native float.
    :return: the FLOAT type according to the specification.
    :raise: TypeError if x is not a float or is NaN.
    """
    if math.isnan(f) or not isinstance(f, float):
        raise TypeError
    if f > sys.float_info.max:
        return sys.float_info.max
    if f < sys.float_info.min:
        return sys.float_info.min
    return f


def toFLOAT2(f: float):
    """
    Convert a native float type to the FLOAT2 type in specification.

    :param f: the native float.
    :return: the FLOAT2 type according to the specification.
    :raise TypeError: if x is not a float or NaN.
    """
    return round(toFLOAT(f), 8)


def matchSTR(s: str):
    """
    Check the native str to match the STR type in specification.

    :param s: the native str.
    :raise TypeError: if s is not a str.
    :raise ValueError: if s contains the char ';'.
    """
    if not isinstance(s, str):
        raise TypeError
    if s.count(';') != 0:
        raise ValueError


def matchINT(i: int):
    """
    Check the native int to match the INT type in specification.

    :param i: the native int.
    :raise TypeError: if i is not an int.
    :raise ValueError: if i if out of bounds (not in
        [-sys.maxsize - 1;sys.maxsize]).
    """
    if not isinstance(i, int):
        raise TypeError
    if i < -sys.maxsize - 1 or i > sys.maxsize:
        raise ValueError


def matchFLOAT(f: float):
    """
    Check the native float to match the FLOAT type in specification.

    :param f: the native float.
    :raise TypeError: if x is not a float or is NaN.
    :raise ValueError: if x is out of bounds (not in
        [sys.float_info.min;sys.float_info.max]).
    """
    if math.isnan(f) or not isinstance(f, float):
        raise TypeError
    if f < sys.float_info.min or f > sys.float_info.max:
        raise ValueError


def matchFLOAT2(f: float):
    """
    Check the native float to match the FLOAT2 type in specification.

    :param f: the native float.
    :raise TypeError: if x is not a float or NaN.
    :raise ValueError: if x is out of bounds OR have to much decimals (>8).
    """
    matchFLOAT(f)
    if round(f, 8) != f:
        raise ValueError


def FLOAT2toSTR(f: float):
    """
    Format a FLOAT2 according to the (hidden and not wrote) specification.
    Notice it doesn't check if f matches FLOAT2 specification because of its
    mistake in rounding.

    :param f: (FLOAT2) the float to get a STR.
    :return: (STR) the STR of the float.
    """
    iF = int(f)
    if iF == f:
        return f'{iF}'
    else:
        return format(f, f'.8f').rstrip("0")
