#!/usr/bin/env python
"""
Ex01 main code.
"""

import csv
import sys

sys.path.append("..")
import specType  # noqa


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def ex01(t: float, d: float):
    """
    Compute the speed.

    :param t: (FLOAT) time, in seconds.
    :param d: (FLOAT) distance, in meters.
    :return: (FLOAT2) speed, in m.s^-1.
    :raise TypeError: if any input doesn't match FLOAT specification.
    :raise ValueError: if any input is out of bounds (not in
        ([sys.float_info.min;sys.float_info.max]).
    """
    specType.matchFLOAT(d)
    specType.matchFLOAT(t)
    s = d / t
    return specType.toFLOAT2(s)


if __name__ == '__main__':
    try:
        # Set file input
        if len(sys.argv) != 2:
            raise ValueError
        else:
            filename = sys.argv[1]

        # Parse file and evaluate input
        with open(filename, newline='') as csvFile:
            reader = csv.reader(csvFile, delimiter=';')
            for row in reader:
                if len(row) != 2:
                    raise ValueError
                try:
                    print(specType.FLOAT2toSTR(
                            ex01(float(row[0]), float(row[1]))
                    ))
                except TypeError:           # Argument is not a FLOAT
                    print(f'ERROR')
                except ValueError:          # Value out of bounds
                    print(f'ERROR')
        csvFile.close()

    except FileNotFoundError:   # Provided file name is not valid
        print(f'ERROR')
    except ValueError:          # Did not provide file name/incorrect struct
        print(f'ERROR')
