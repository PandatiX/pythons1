#!/usr/bin/env python

import math
import sys
import unittest

from ex01.main import ex01


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx01(unittest.TestCase):
    def testEx01(self):
        # Test OK
        self.assertEqual(0.33333333, ex01(3.0, 1.0))
        self.assertEqual(sys.float_info.max, ex01(sys.float_info.min,
                                                  sys.float_info.max))
        # Returning value out of FLOAT2 bounds
        self.assertEqual(0.0, ex01(sys.float_info.max, sys.float_info.min))

        # Test Error
        # Notice <int> can't be a parameter type, but float(<int>)
        # is fine in a file input strategy
        with self.assertRaises(TypeError):      # Triggers 1st matchFloat
            ex01(math.nan, 1.0)
        with self.assertRaises(TypeError):
            ex01(None, 1.0)
        with self.assertRaises(TypeError):
            ex01(1, 1.0)
        with self.assertRaises(TypeError):
            ex01(f'1.0', 1.0)
        with self.assertRaises(ValueError):
            ex01(0.0, 1.0)
        with self.assertRaises(ValueError):
            ex01(math.inf, 1.0)
        with self.assertRaises(TypeError):      # Triggers 2nd matchFloat
            ex01(1.0, math.nan)
        with self.assertRaises(TypeError):
            ex01(1.0, None)
        with self.assertRaises(TypeError):
            ex01(1.0, 1)
        with self.assertRaises(TypeError):
            ex01(1.0, f'1.0')
        with self.assertRaises(ValueError):
            ex01(1.0, 0.0)
        with self.assertRaises(ValueError):
            ex01(1.0, math.inf)


if __name__ == '__main__':
    unittest.main()
