#!/usr/bin/env python

import math
import unittest

from ex02.main import ex02


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx02(unittest.TestCase):
    def testEx02(self):
        # Test OK
        self.assertEqual(5.0, ex02(25.0))
        self.assertEqual(1.41421356, ex02(2.0))

        # Test KO
        # Not matching FLOAT spec (number of decimals)
        self.assertNotEqual(1.4, ex02(2.0))

        # Test Error
        # Notice <int> can't be a parameter type, but float(<int>)
        # is fine in a file input strategy
        with self.assertRaises(TypeError):
            ex02(math.nan)
        with self.assertRaises(TypeError):
            ex02(None)
        with self.assertRaises(TypeError):
            ex02(1)
        with self.assertRaises(TypeError):
            ex02(f'1.0')
        with self.assertRaises(ValueError):
            ex02(0.0)
        with self.assertRaises(ValueError):
            ex02(math.inf)


if __name__ == '__main__':
    unittest.main()
