# !/usr/bin/env python
"""
Ex02 main code.
"""

import csv
import sys
import math

sys.path.append("..")
import specType  # noqa


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def ex02(n: float):
    """
    Return the square root of a FLOAT.

    :param n: (FLOAT) number to set to square root.
    :return: (FLOAT2) square root of n.
    :raise TypeError: if any input doesn't match FLOAT specification.
    :raise ValueError: if any input is out of FLOAT bounds.
    """
    specType.matchFLOAT(n)
    s = math.sqrt(n)                # FLOAT spec: n > 0
    return specType.toFLOAT2(s)


if __name__ == '__main__':
    try:
        # Set file input
        if len(sys.argv) != 2:
            raise ValueError
        else:
            filename = sys.argv[1]

        # Parse file and evaluate input
        with open(filename, newline='') as csvFile:
            reader = csv.reader(csvFile, delimiter=';')
            for row in reader:
                if len(row) != 1:
                    raise ValueError
                try:
                    print(specType.FLOAT2toSTR(
                        ex02(float(row[0]))
                    ))
                except TypeError:           # Argument is not a FLOAT
                    print(f'ERROR')
                except ValueError:          # Value out of bounds
                    print(f'ERROR')
        csvFile.close()

    except FileNotFoundError:   # Provided file name is not valid
        print(f'ERROR')
    except ValueError:          # Did not provide file name/incorrect struct
        print(f'ERROR')
