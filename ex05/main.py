#!/usr/bin/env python
"""
Ex05 main code.
"""


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


def ex05():
    """
    Build a range of integers from 0 to 15 (both not included), with a step of
    3.

    :return: [INT, ...] the range.
    """
    return range(1, 15, 3)


if __name__ == '__main__':
    print(list(ex05()))
