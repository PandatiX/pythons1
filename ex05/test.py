#!/usr/bin/env python

import unittest

from ex05.main import ex05


__author__ = "Lucas TESSON (e2004675), Nolan GLOUX (e2004726)"
__copyright__ = "Copyright 2020, Lucas TESSON (e2004675)," \
                "Nolan GLOUX (e2004726)"
__credits__ = ["Lucas Tesson", "Nolan Gloux"]
__maintainer__ = "Lucas Tesson"
__email__ = "lucasfloriantesson@gmail.com"
__status__ = "Development"


class TestEx05(unittest.TestCase):
    def testEx05(self):
        lst = list(ex05())
        # Test OK
        self.assertEqual([1, 4, 7, 10, 13], lst)

        # Test KO
        self.assertNotEqual([0, 3, 6, 9, 12], lst)
        self.assertNotEqual([2, 5, 8, 11, 14], lst)
        self.assertNotEqual([3, 6, 9, 12, 15], lst)


if __name__ == '__main__':
    unittest.main()
